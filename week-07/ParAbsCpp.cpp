//29 week 7
#include<iostream>
using namespace std;
class x
{
    public:
    void msg();
};
class n : public x
{
    public:
    void msg()
    {
        cout<<"Hello java"<<endl;
    }
};
int main()
{
    n obj;
    obj.msg();
}
/*
observation
here we have to declare our function in parent class, 
and we have to define it in our child class in impure abstraction
*/