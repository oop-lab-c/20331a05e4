//31 week7
class MethodOlJava
{
    void show(int a, int b)
    {

        System.out.println(a*b);
    }
    void show(int a, int b,int c)
    {
        System.out.println(a*b*c);
    }
    public static void main(String[] args)
    {
        MethodOlJava obj = new MethodOlJava();
        obj.show(30,10);
        obj.show(55,77,50);
    }
}
/*
observation
method overloading is based on different parameters list.*/