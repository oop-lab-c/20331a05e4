//19 week5
class Parent
{
    void msg()
    {
        System.out.println("This is parent class ");
    }
}
class SimpleInherJava extends Parent
{
    void msg1()
    {
        System.out.println("This is child class ");
    }
    public static void main(String args[])
    {
        SimpleInherJava obj = new SimpleInherJava();
        obj.msg();
        obj.msg1();
    }
}
/*
observation
   This is simple inheritance because in this only one super class , one base classw was present.*/