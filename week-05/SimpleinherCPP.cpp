#include<iostream>
using namespace std;
class a{
    public:
    void fun1(){
        cout<<"I am parent and this is simple inheritance"<<endl;
    }
};
class b : public a{
    public:
    void fun(){
        cout<<"I am children "<<endl;
    }
};
int main(){
    b obj;
    obj.fun();
    obj.fun1();
    return 0;
}

/*
observation
here we created only object for child class
it access both the methods of parent and child.
*/