#include<iostream>
using namespace std;
class Person { //class Person
public:
    Person(int x)  { cout << "Person is  called" << endl; }
};
 
class Father : public Person { //class Father inherits Person
public:
    Father(int x):Person(x)   {
       cout << "Father is called" << endl;
    }
};
 
class Mother : public Person { //class Mother inherits Person
public:
    Mother(int x):Person(x) {
        cout << "Mothe is called" << endl;
    }
};
 
class Child : public Father, public Mother  { //Child inherits Father and Mother
public:
    Child(int x):Mother(x), Father(x)   {
        cout << "Child is called" << endl;
    }
};
 
int main() {
    Child child(30);
}