//17 week5
#include<iostream>
using namespace std;
class par1
{
    public:
    void fun()
    {
        cout<<"I am grand parent1"<<endl;
    }
};
class par2
{
    public:
    void fun1()
    {
        cout<<"I am grandparent2"<<endl;
    }
};
class c : public par1
{
    public:
    void fun2()
    {
        cout<<"This was simple inheritance"<<endl;
    }
};
class d :public par1,par2
{
    public:
    void fun3()
    {
        cout<<"This was multiple inheritance"<<endl;
    }
};
class e :public c
{
    public:
    void fun4()
    {
        cout<<"This was multilevel inhreitance"<<endl;
    }
};
class f :public c
{
    public:
    void fun5()
    {
        cout<<"This was hierarchical inheritance"<<endl;
    }
};
int main()
{
    c obj ;
    obj.fun2();
    obj.fun();
    d obj1 ;
    obj1.fun();
    obj1.fun3();
    e obj2;
    obj2.fun();
    obj2.fun2();
    obj2.fun4();
    f obj3;
    obj3.fun();
    obj3.fun2();
    obj3.fun5();

}
